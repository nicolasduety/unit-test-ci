import { MyValidators } from './validators';
import { FormControl } from '@angular/forms';

describe("Test for validators", ()=>{

  describe("Test for skuValidate", ()=>{

    it("should return null for '1234'", ()=>{
      let formControl = new FormControl();
      formControl.setValue("1234");
      let rta = MyValidators.skuValidate(formControl);
      expect(rta).toBeNull();
    });

    it("should return null for ''", ()=>{
      let formControl = new FormControl();
      formControl.setValue("");
      let rta = MyValidators.skuValidate(formControl);
      expect(rta).toBeNull();
    });

    it("should return an error for '543121'", ()=>{
      let formControl = new FormControl();
      formControl.setValue("543121");
      let rta = MyValidators.skuValidate(formControl);
      expect(rta.invalidSku).toBeTruthy();
    });

  });

});